<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'cia' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'EKuoFZp-l$;M~L~L+`xG2wZeqpg%`=hjGbFm2lH?=Cm)z@bg9-j~,&S5O|,X=A5w' );
define( 'SECURE_AUTH_KEY',  '50ET(I(,(p*RlHv<$s*U-:[cpp`(9=Pf.B%+p8&u&]t2`clOg>>/GOhy9`bC`(Zg' );
define( 'LOGGED_IN_KEY',    'gN=*%2.o=C1<`ofraFfogypNTqu703@&0JGo$&.lvX#c57:PAQ1C:C*2)V$!bb1 ' );
define( 'NONCE_KEY',        '/p&!-b+M`8c*RH`nJGQ4LoXGl4,QnB<w`/L]IcmpzBM&uw}X@^Pa_EaVnx :ZPSy' );
define( 'AUTH_SALT',        'Y(tP5XEz4q}~k,{RM(%W6jP!tNlZ:G?Qsst@0M2bq%NBfe.rHg+J`lxuzd2R51WD' );
define( 'SECURE_AUTH_SALT', '8cJ%vciW@vP~V^OLWEg$?MaDxZIYAe8)J5h}Rj[=qZI;HG$ytDk*eAd0PMREPD?G' );
define( 'LOGGED_IN_SALT',   'ay:H,C*#htWvI>b/}C)?K?S?RGUYLXXaCg.|C&_9).&:MXTI!.wwj6md@Q^=v<$U' );
define( 'NONCE_SALT',       'h2JFwg2l3ud6H~uZCMo=hw&m iQ4jDQ~k;^B3;Ax89QF>3y~Bg6Z<Auf2ZU<cS}S' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
